# prettier-standard-example

A tiny example of a JS app with the linter/formatter
[prettier-standard][].

This setup works well the Vim plugin [ALE][ale] with the following .vimrc config:

```vimscript
let g:ale_fixers = {'javascript': ['prettier_standard']}
```

[ale]: https://github.com/dense-analysis/ale
[prettier-standard]: https://github.com/sheerun/prettier-standard
